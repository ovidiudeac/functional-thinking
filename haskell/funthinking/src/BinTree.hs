module BinTree where

import Data.List (unfoldr)

data Tree t = Empty | Node t (Tree t) (Tree t)

append :: (Ord t) => t -> Tree t -> Tree t
append v Empty = Node v Empty Empty
append v (Node n left right) =
    if v > n then Node n left (append v right)
    else Node n (append v left) right

ofList :: (Ord t) => [t] -> Tree t
ofList xs = foldr append Empty xs

inOrder :: Tree t -> [t]
inOrder Empty = []
inOrder (Node v left right) = (inOrder left) ++ [v] ++ (inOrder right)

