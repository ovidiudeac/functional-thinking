{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module AdjList where

import qualified Graph as G

instance G.Graph [(t,[t])] t where
    vertices g = map fst g

    adj g v = concatMap snd $ filter startsFromV g
        where startsFromV (start, _) = start == v


g :: [(Int, [Int])]
g = [
        (0, [1,2,5]),
        (1, [0,2]),
        (2, [1,3,4]),
        (3, [2,4,5]),
        (4, [2,3]),
        (5, [1,3])
    ]

vs :: [Int]
vs = G.vertices g

dfs :: [[Int]]
dfs = G.dfs g 0

bfs :: [[Int]]
bfs = G.bfs g 0

main = do
    putStrLn "AdjList ------------------  "
    putStrLn $ "Dfs 0:" ++ (show dfs)
    putStrLn $ "Bfs 0:" ++ (show bfs)