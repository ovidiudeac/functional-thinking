{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Maze where

import qualified Graph as G


type Coord = (Int, Int)

size :: [[a]] -> (Int, Int)
size matrix = (length matrix, length $ head matrix)

a `inInterval` (low, high) = a >= low && a < high

instance G.Graph [[Char]] Coord where
    vertices matrix =
        [ (row, column) |
            row <- [0..height-1],
            column <- [0..width-1],
            (matrix !! row !! column) /= '.']
        where (height,width) = size matrix

    adj matrix (x,y) = filter inMatrix cellNeighbours
        where (height,width) = size matrix
              inMatrix (r, c) = c `inInterval` (0, width)
                                    && r `inInterval` (0, height)
                                    && ('.' /= (matrix !! x !! y))
              cellNeighbours = [(x + dx, y + dy) | (dx,dy) <- [(-1,0), (0,-1), (0,1), (1,0)] ]

findPath :: [[Char]] -> Coord -> [[Coord]]
findPath matrix start =
    filter endsWithX $ G.bfs matrix start
    where endsWithX ((x,y):rest) = matrix !! x !! y == 'x'



maze = [
        "ooooooo",
        "o..o..o",
        "o ooooo",
        "o.x.o.o",
        "o.o.o.o",
        "ooooooo"
    ]

main = do
    putStrLn "Maze ------------------  "
    putStrLn $ "Find path:" ++ ( show $ findPath maze (0,0))