{-# LANGUAGE MultiParamTypeClasses #-}

module Graph (Graph, vertices, adj, bfs, dfs) where

import qualified Data.Set as S

type Path a = [a]
type CandidateList a = [(a, Path a)]

data SearchState a = SearchState {
        vertex :: a,
        path :: [a],
        visitedVertices :: S.Set a,
        candidates :: CandidateList a
    }

type ConcatenateList a = CandidateList a -> CandidateList a -> CandidateList a

search :: (Ord a, Eq a, Graph g a) => g -> ConcatenateList a -> SearchState a -> [Path a]
search graph enqueueCandidates currentState = (path currentState) : rest
    where allAdj = adj graph (vertex currentState)
          newCandidates = map (\v -> (v, (path currentState))) allAdj
          allCandidates = enqueueCandidates (candidates currentState) newCandidates

          notVisited (x, _) = S.notMember x (visitedVertices currentState)

          rest = case filter notVisited allCandidates of
            [] -> []
            (firstCandidate, candidatePath) : restCandidates ->
                search graph enqueueCandidates nextState where
                    nextPath = firstCandidate : candidatePath
                    nextVisited = S.insert firstCandidate (visitedVertices currentState)

                    nextState = SearchState firstCandidate nextPath nextVisited restCandidates

fifoEnqueue old new = new ++ old
lifoEnqueue old new = old ++ new

class Graph g a where
    vertices :: g -> [a]

    adj :: Eq a => g -> a -> [a]

    bfs :: (Ord a, Eq a) => g -> a -> [[a]]
    bfs graph start = search graph lifoEnqueue initialState where
        initialState = SearchState start
                                [start]
                                (S.singleton start)
                                []
    dfs :: (Ord a, Eq a) =>g -> a -> [[a]]
    dfs graph start = search graph fifoEnqueue initialState where
        initialState = SearchState start
                                [start]
                                (S.singleton start)
                                []
