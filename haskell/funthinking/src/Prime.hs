module Prime where

seq :: [Integer]
seq = sieve [2..]
    where sieve (head : rest) = head : filter (not . isDivisibleBy head) rest
          isDivisibleBy x y = (y `mod` x) == 0