{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Router where

import qualified Graph as G
import qualified Data.Set as S

type Connection = (String, String)
type Node = String
type Route = [Node]

network :: [Connection]
network = [
        ("cj", "bv"),
        ("cj", "tm"),
        ("b", "bv"),
        ("b", "ct"),
        ("bv","is"),
        ("tm", "cj")
    ]

instance G.Graph [Connection] Node where
    vertices :: [Connection] -> [Node]
    vertices net = S.toList $ S.fromList $ concatMap (\(n1,n2) -> [n1,n2]) net

    adj :: [Connection] -> Node -> [Node]
    adj net node = map otherNode $ filter containsNode net
        where containsNode (n1, n2) = n1 == node || n2 == node
              otherNode (n1, n2) = if node == n1 then n2
                                   else n1

findRoute :: [Connection] -> Node -> Node -> Route
findRoute network start end =
    head $ filter (\(routeEnd: rest) -> routeEnd == end) $ G.bfs network start

nodes :: [Node]
nodes = G.vertices network

main = do
    putStrLn "Router ------------------  "
    putStrLn $ "Nodes:" ++ (show nodes)
    putStrLn $ "Adj bv:" ++ (show $ G.adj network "bv")
    putStrLn $ "Find route:" ++ ( show $ findRoute network "cj" "ct")
