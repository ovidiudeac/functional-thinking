module Fibonacci where

import Data.List (unfoldr)

seq :: [Integer]
seq = unfoldr folder (1,1)
    where folder (a,b) = Just(a, nextState)
            where nextState = (b, (a+b))