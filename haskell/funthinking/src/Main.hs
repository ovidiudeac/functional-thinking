{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Main where

import qualified Fibonacci as F
import qualified Prime as P
import qualified BinTree as BT
import qualified Maze
import qualified AdjList
import qualified Router


network = []

main = do
    putStrLn $ "Fibs: " ++ (show $ take 10 F.seq)
    putStrLn $ "Primes: " ++ (show $ take 10 P.seq)
    putStrLn $ "Tree: " ++ (show $ BT.inOrder $ BT.ofList [1,7,8,4,1])
    AdjList.main
    Maze.main
    Router.main

