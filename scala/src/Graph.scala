package Graph

trait Graph[A] {
  type CandidateList = List[Tuple2[A, List[A]]]

  val vertices : List[A]
  def adj(vertex : A) : List[A]

  def bfs(start:A) : Stream[List[A]] = search (lifoEnqueue, SearchState(start, List(start), Set(start), List.empty))

  def dfs(start:A) : Stream[List[A]] = search (fifoEnqueue, SearchState(start, List(start), Set(start), List.empty))

  def search(enqueue: (CandidateList, CandidateList) => CandidateList,
             currentState: SearchState): Stream[List[A]] =
    {
      val adjCandidates = adj(currentState.node).map( v => Tuple2(v, currentState.path))
      val allCandidates = enqueue(currentState.candidates, adjCandidates)

      def notVisited(candidate:Tuple2[A, List[A]]) = ! currentState.visitedVertices.contains(candidate._1)

      val rest = allCandidates.filter(notVisited) match {
        case List() => Stream.empty
        case firstCandidate :: restCandidates =>
        {
          val nextPath = firstCandidate._1 :: firstCandidate._2
          val nextVisited = currentState.visitedVertices + firstCandidate._1
          val nextState = SearchState(firstCandidate._1, nextPath, nextVisited, restCandidates)
          search(enqueue, nextState)
        }
      }
      currentState.path #:: rest
    }


  case class SearchState(node:A, path:List[A], visitedVertices: Set[A], candidates : CandidateList)

  def lifoEnqueue(oldCandidates: CandidateList, newCandidates:CandidateList) = oldCandidates ++ newCandidates
  def fifoEnqueue(oldCandidates: CandidateList, newCandidates:CandidateList) = newCandidates ++ oldCandidates

}

