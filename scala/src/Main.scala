import Fibonacci._
import Prime._
import BinTree._


object Main {


  def main(args : Array[String]): Unit = {
    println("fibonacci = " + Fibonacci.stream.take(10).mkString(","))
    println("primes = " + Prime.stream.take(10).mkString(","))
    println("Sorted ints: " + BinTree.of(1,5,3,2,7,2,1).stream.take(3).mkString(","))

    AdjList.main

    println ("End")

  }
}