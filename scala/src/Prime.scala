package Prime

object Prime {
  private def sieve (candidates:Stream[Int]) : Stream[Int] =
    candidates.head #:: candidates.tail.filter(n => n%candidates.head != 0)

  val stream : Stream[Int] = sieve(Stream.from(2))
}
