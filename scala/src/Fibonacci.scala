package Fibonacci

object Fibonacci {
  val stream : Stream[Int] = 1 #:: 2 #:: stream.zip(stream.tail).map {case (x,y) => x + y}
}
