package BinTree

trait BinTree {
  def add(v : Int) : BinTree
  def isEmpty : Boolean

  def stream : Stream[Int]
}

case class NonEmptyTree(v:Int, left: BinTree, right : BinTree) extends BinTree {
  override def add(v: Int): BinTree =
    if (v > this.v) new NonEmptyTree(this.v, left, right.add(v))
    else new NonEmptyTree(this.v, left.add(v), right)

  override def isEmpty: Boolean = false

  override def stream: Stream[Int] = {
    //println("Visiting " + v)
    Stream.concat(left.stream, v #:: right.stream)
  }
}

case class EmptyTree() extends BinTree {
  override def add(v: Int): BinTree = new NonEmptyTree(v, BinTree.empty, BinTree.empty)

  override def isEmpty: Boolean = true

  override  def stream : Stream[Int] = Stream.empty
}

object BinTree {
  val empty : BinTree = EmptyTree()
  def singleton(v : Int) = NonEmptyTree(v, empty, empty)

  def of(ints : Seq[Int]) : BinTree =
    ints.foldLeft (empty) ((t : BinTree, v : Int) => t.add(v))

  def of(first : Int, rest: Int*) : BinTree =
    rest.foldLeft (singleton(first) : BinTree) ((t : BinTree, v : Int) => t.add(v)) //todo how to reuse of(Seq[Int])
}