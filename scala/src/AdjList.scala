import Graph.Graph

case class Adj(vertex:Int, adjs:List[Int])

case class AdjList(items : Adj*) extends Graph[Int] {
  override val vertices: List[Int] = items.map {_.vertex}
                                          .toList

  override def adj(vertex: Int): List[Int] =
    items.filter ( item => item.vertex == vertex)
         .flatMap{_.adjs}
         .toList
}

object AdjList {
  def main = {
    println("Graph ----------------------")

    val g = AdjList( Adj(0, List(1,2,5)),
                     Adj(1, List(0,2)),
                     Adj(2, List(1,3,4)),
                     Adj(3, List(2,4,5)),
                     Adj(4, List(2,3)),
                     Adj(5, List(1,3)))

    println("Dfs: " + g.dfs(0).mkString(","))
    println("Bfs: " + g.bfs(0).mkString(","))
  }
}