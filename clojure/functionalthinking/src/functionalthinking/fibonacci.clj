(ns functionalthinking.fibonacci)


(def fibs
  (map first
       (iterate
           (fn [[ a b]]
                [b (+ a b) ])
           [1 2])))

(defn -generate-fibs [a b]
  (lazy-seq (cons a
                  (-generate-fibs b (+ b a)))))

(def fibs2 (-generate-fibs 1 2))
