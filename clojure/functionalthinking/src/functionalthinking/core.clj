(ns functionalthinking.core
  (:require [functionalthinking.fibonacci :as f]
            [functionalthinking.prime :as p]))

(defn -main [ & args ]
  (->>
    f/fibs
    (take 5 )
    (println "fibs: "))

  (->>
    f/fibs2
    (take 5 )
    (println "fibs2: "))
  (->>
    p/primes
    (take 10 )
    (println "primes: "))
    )
