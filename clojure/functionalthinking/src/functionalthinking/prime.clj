(ns functionalthinking.prime)

(def numbers (iterate (fn [x] (+ x 1)) 2))

(defn -is-divisible-by [divisor]
  (fn [n] (not= 0 (mod n divisor))))

(defn -generate-primes [s]
  (let [ head (first s)
         tail (rest s)]

  (cons head
        (lazy-seq (-generate-primes (filter (-is-divisible-by head) tail))))))

(def primes (-generate-primes numbers))
