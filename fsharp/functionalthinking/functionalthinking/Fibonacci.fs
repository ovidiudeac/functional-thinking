﻿module ft.fib

let rec private impl a b=
    seq {
        yield a
        yield! impl b (a + b)
    }


let numbers = impl 1 2