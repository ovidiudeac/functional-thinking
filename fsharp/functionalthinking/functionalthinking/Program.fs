﻿open ft
open ft.graph

[<EntryPoint>]
let main argv = 
    fib.numbers
    |> Seq.take 10
    |> List.ofSeq
    |> printfn "Fibonacci: %A"

    square.numbers
    |> Seq.take 10
    |> List.ofSeq
    |> printfn "Squares: %A"

    prime.numbers
    |> Seq.take 100
    |> List.ofSeq
    |> printfn "Primes: %A"

    [9;2;7;3;4;5;7;12;3;1;245;999;22]
    |> bt.ofSeq
    |> bt.inorder
    |> List.ofSeq
    |> printfn "sortedNumbers: %A"

    printfn "Graph -----------------"
    let toGraph g =
        {
            new Graph<int> with
                member this.vertices = List.map fst g
                member this.adj v = 
                    g
                    |> Seq.filter (fun (vertex,adjs) -> vertex = v)
                    |> Seq.collect snd
                    |> List.ofSeq

        }

    let g = 
        toGraph [
            (0, [1;2;5])
            (1, [0;2])
            (2, [1;3;4])
            (3, [2;4;5])
            (4, [2;3])
            (5, [1;3])
        ]

    
    g.vertices
    |> printfn "Vertices: %A" 
    
    dfs g 0
    |> List.ofSeq
    |> printfn "DFS: %A" 

    bfs g 0
    |> List.ofSeq
    |> printfn "BFS: %A" 
    0

