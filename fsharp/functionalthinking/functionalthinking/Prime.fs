﻿module ft.prime

let rec generate (candidates:int seq) =
    seq {
        let p = Seq.head candidates
        let remainingCandidates = 
            Seq.skip 1 candidates
            |> Seq.filter (fun x -> (x % p) <> 0)

        yield p
        yield! generate remainingCandidates
    }

let numbers = 
    Seq.initInfinite id
    |> Seq.skip 2
    |> generate
