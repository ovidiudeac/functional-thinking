﻿module ft.graph

type Graph<'t> = 
    abstract vertices : 't list
    abstract adj : 't -> 't list

module private impl =
    type CandidateList<'t> = ('t * ('t list)) list
    type ConcatenateList<'t> = CandidateList<'t> -> CandidateList<'t> -> CandidateList<'t>

    type SearchState<'t when 't : comparison> = 
        {
            vertex : 't
            path : 't list
            visitedVertices : Set<'t>
            candidates : CandidateList<'t>
        }

        static member init v = {
                vertex = v 
                path = [v] 
                visitedVertices = Set.ofList [v]
                candidates = []
            }


    let rec search (g : Graph<'t>) 
                   (enqueue : ConcatenateList<'t>)
                   (currentState : SearchState<'t>) : 't list seq =
        seq {
            yield currentState.path
            let newCandidates = 
                g.adj currentState.vertex
                |> List.map ( fun v -> (v, currentState.path))

            let notVisitedCandidates =
                enqueue currentState.candidates newCandidates
                |> List.filter (fst >> currentState.visitedVertices.Contains >> not)

            match notVisitedCandidates with
            | [] -> yield! Seq.empty
            | (firstCandidate, candidatePath) :: restCandidates ->
                let nextState =  {
                        vertex = firstCandidate
                        path = firstCandidate :: candidatePath
                        visitedVertices = Set.add firstCandidate currentState.visitedVertices
                        candidates = restCandidates
                    }
                yield! search g enqueue nextState
        }
        
    let fifoEnqueue old new' = List.append new' old
    let lifoEnqueue old new' = List.append old new' 

open impl

let dfs (g : Graph<'t>) (start : 't) : 't list seq = search g fifoEnqueue (SearchState<'t>.init start)

let bfs (g : Graph<'t>) (start : 't) : 't list seq = search g lifoEnqueue (SearchState<'t>.init start)


